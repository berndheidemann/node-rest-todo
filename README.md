# Developing a RESTful API With Node and TypeScript

## Want to learn how to build this project?

Check out the [blog post](http://mherman.org/blog/2016/11/05/developing-a-restful-api-with-node-and-typescript/#.WB3zyeErJE4).

## Want to use this project?


Wichtig: 
    npm install
    npm install -g nodemon
    npm install -g gulp

Compile �ber "gulp" (laufen lassen)

Server starten mit "npm start"

dann Browser --> localhost:3000/todos

Sqlite Referenz: https://www.npmjs.com/package/sqlite-sync


## Sample Projects

- [Simple whois REST API](https://github.com/wingsuitist/whoissv)
