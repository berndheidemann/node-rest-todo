import { Todo } from '../model/Todo';
import { Category } from '../model/Category';

// var sqlite3 = require('sqlite3').verbose();

const sqlite = require('sqlite-async')


export class TodoDAO {

    static dbFile = 'databaseTodoApp.db3';

    static async save(todo: Todo) {
        let db = await sqlite.open(TodoDAO.dbFile);
        try {

            await db.run(`insert into todo VALUES ((select max(id) from todo)+1, ?, 0, ?, ?, ?)`, [todo.label, todo.category.id, todo.dueDate, todo.priority]);
        } catch (e) {
            console.log(e);
            return e;
        } finally {
            db.close();
        }
    }


    static async getAll(): Promise<Todo[]> {
        let db = await sqlite.open(TodoDAO.dbFile);
        let todos = await db.all("SELECT todo.id, todo.label, todo.done, todo.dueDate, todo.priority, category.id as 'catid', category.label as 'catlabel' FROM todo join category on todo.category_id = category.id");
        let todoList: Todo[] = [];
        todos.forEach(t => {

            todoList.push(new Todo(t.id, t.label, t.done, new Category(t.catid, t.catlabel), t.dueDate, t.priority));
        });
        db.close();
        return todoList;
    }




    static async getById(id: number): Promise<Todo> {
        let db = await sqlite.open(TodoDAO.dbFile);
        let todo = db.get(`select * from todo where id = ?`, id);
        db.close();
        return todo;
    }

    static async edit(todo: Todo) {
        let db = await sqlite.open(TodoDAO.dbFile);
        try {
            await db.run(`update todo set label = ?, done=? where id = ?`, [todo.label, todo.done, todo.id]);
        } catch (e) {
            console.log(e);
            return e;
        } finally {
            db.close();
        }
    }

    static async patch(todo: Todo) {
        let db = await sqlite.open(TodoDAO.dbFile);
        try {
            let query = `update todo set `;
            if (todo.label) {
                query += `label = '${todo.label}'`;
            } else if (todo.done) {
                query += `done=${todo.done}`;
            }
            query += ` where id = ${todo.id}`;
            console.log(query);
            await db.run(query);
        } catch (e) {
            console.log(e);
            return e;
        } finally {
            db.close();
        }
    }

    static async delete(id: number) {
        let db = await sqlite.open(TodoDAO.dbFile);
        try {
            await db.run(`delete from todo where id = ?`, id);
        } catch (e) {
            console.log(e);
            return e;
        } finally {
            db.close();
        }
    }


}