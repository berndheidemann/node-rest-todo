import { Category } from '../model/Category';
const sqlite = require('sqlite-async');

export class CategoryDAO {

    static dbFile = 'databaseTodoApp.db3';

    static async getById(id: number): Promise<Category> {
        let db = await sqlite.open(CategoryDAO.dbFile);
        let category = db.get(`select * from category where id = ?`, id);
        db.close();
        return category;
    }

    static async getAll() {
        let db = await sqlite.open(CategoryDAO.dbFile);
        let cats = await db.all('select * from Category');
        db.close();
        return cats;
    }

}