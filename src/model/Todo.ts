import { Category } from './Category';
export class Todo {

    constructor(public id?: number, public label?: string, public done?: number, public category?: Category, public dueDate?: Date, public priority?: number) {

    }
}