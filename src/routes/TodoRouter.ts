import { Router, Request, Response, NextFunction } from 'express';
import { Todo } from '../model/Todo';
import { TodoDAO } from '../dao/TodoDAO';
import { Category } from '../model/Category';
import { CategoryDAO } from '../dao/CategoryDAO';



export class TodoRouter {
  router: Router

  constructor() {
    this.router = Router();
    this.init();
  }


  async getAll(req: Request, res: Response, next: NextFunction) {
    let todoList = await TodoDAO.getAll();

    if (todoList) {
      res.status(200).send(JSON.stringify(todoList));
    }
    else {
      res.status(404).send('nix gefunden');
    }
  }

  async getOne(req: Request, res: Response, next: NextFunction) {
    let todo = await TodoDAO.getById(req.params.id);

    if (todo) {
      res.status(200).send(JSON.stringify(todo));
    }
    else {
      res.status(404).send('nix gefunden');
    }
  }

  async post(req: Request, res: Response, next: NextFunction) {
    console.log(req.body);
    let category: Category = await CategoryDAO.getById(req.body.category_id);
    if (category) {
      let todo: Todo = new Todo(req.body.id, req.body.label, req.body.done, category, req.body.dueDate, req.body.priority);
      let err = await TodoDAO.save(todo);
      if (!err) {
        res.status(200).send("success");
      }
      else {
        console.log("speicherversuch bei " + JSON.stringify(todo));
        res.status(500).send("could not be saved");
      }
    } else {
      res.status(405).send("category doesnt exist or not given");
    }
  }

  async put(req: Request, res: Response, next: NextFunction) {
    let todo = TodoDAO.getById(req.params.id);
    if (!todo) {
      res.status(405).send("id existiert nicht");
    }
    else {
      let todo: Todo = new Todo(req.params.id, req.body.label, req.body.done);

      let err = await TodoDAO.edit(todo);
      if (err) {
        res.status(500).send("could not be edited");
      }
      else {
        res.status(200).send("success");
      }
    }

  }

  async patch(req: Request, res: Response, next: NextFunction) {
    let todo = await TodoDAO.getById(req.params.id);
    if (!todo) {
      res.status(405).send("id existiert nicht");
    }
    else {
      let todo: Todo = new Todo(req.params.id);
      if (req.body.label) {
        todo.label = req.body.label;
      } else if (req.body.done) {
        todo.done = req.body.done;
      } else {
        res.status(405).send("keine Änderung im Body vorhanden");
      }
      let err = await TodoDAO.patch(todo);
      if (err) {
        res.status(500).send("could not be edited");
      }
      else {
        res.status(200).send("success");
      }
    }

  }

  async delete(req: Request, res: Response, next: NextFunction) {
    let todo = await TodoDAO.getById(req.params.id);
    if (!todo) {
      res.status(405).send("id existiert nicht");
    } else {
      let err = await TodoDAO.delete(req.params.id);
      if (!err) {
        res.status(200).send("success");
      }
      else {
        res.status(500).send("geht nicht " + JSON.stringify(err));
      }
    }
  }


  init() {
    this.router.get('/', this.getAll);
    this.router.post('/', this.post)
    this.router.get('/:id', this.getOne);
    this.router.put('/:id', this.put);
    this.router.patch('/:id', this.patch);
    this.router.delete('/:id', this.delete);
  }
}

const todoRoutes = new TodoRouter();
export default todoRoutes.router;
