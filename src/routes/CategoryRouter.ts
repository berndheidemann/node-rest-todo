import { Router, Request, Response, NextFunction } from 'express';
import { Todo } from '../model/Todo';
import { TodoDAO } from '../dao/TodoDAO';
import { Category } from '../model/Category';
import { CategoryDAO } from '../dao/CategoryDAO';



export class CategoryRouter {
  router: Router

  constructor() {
    this.router = Router();
    this.init();
  }


  async getAll(req: Request, res: Response, next: NextFunction) {
    let catList = await CategoryDAO.getAll();

    if (catList) {
      res.status(200).send(JSON.stringify(catList));
    }
    else {
      res.status(404).send('nix gefunden');
    }
  }

  async getOne(req: Request, res: Response, next: NextFunction) {
    // let todo = await TodoDAO.getById(req.params.id);

    // if (todo) {
    //   res.status(200).send(JSON.stringify(todo));
    // }
    // else {
    //   res.status(404).send('nix gefunden');
    // }
  }




  init() {
    this.router.get('/', this.getAll);
    // this.router.get('/:id', this.getOne);

  }
}

const categoryRoutes = new CategoryRouter();
export default categoryRoutes.router;
